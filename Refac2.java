/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

//import java.io.*; NO SE USA Y ADEMÁS IMPORTA TODAS LAS CLASES
import java.util.Scanner; //IMPORTADA SOLO LA CLASE SCANNER

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */

public class Refac2 { //EL NOMBRE DE LA CLASE ES EL DEL FICHERO

	static final NUMERO_ALUMNOS = 10;

	//AÑADIDO SALTO DE LINEA
	public static void main(String[] args) 
	{
		//INICIALIZADO EL SCANNER Y CAMBIADO EL NOMBRE DEL IDENTIFICADOR
		Scanner input = new Scanner(System.in);
		
		//INICIALIZAR VARIABLES CADA UNA EN UNA LÍNEA
		//int n; ESTA VARIABLE SE INICIALIZA EN EL FOR
		int cantidad_maxima_alumnos = 10;;
		
		//AÑADIDOS ESPACIOS Y RENOMBRADAS VARIABLES
		int notaMedia[] = new int[NUMERO_ALUMNOS];
		for(int i = 0; i < NUMERO_ALUMNOS; i++) {
			System.out.println("Introduce nota media de alumno");
			notaMedia[n] = input.nextInt();
		}	
		
		System.out.println("El resultado es: " + calcularMedia(notaMedia));
		
		input.close();
	}
	
	//AÑADIDO SALTO DE LÍNEA Y ESPACIOS
	static double calcularMedia(int vector[]) {
		double media = 0;
		for(int i = 0; i < 10; i++) { //SUSTITUIDO EL INPUT POR INDEX
			media = media + vector[input];
		}
		return media / 10;
	}
	
}