package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS_MAXIMOS = 7;
	private final static String RUTA = "src\\palabras.txt";
	private static byte fallosUsuario = 0;

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);	
		String[] palabras = inicializarFichero();
		String palabraSecreta = elegirPalabraSecreta(palabras);
		char[][] caracteresPalabra = inicializarPalabra(palabraSecreta);
		String caracteresElegidos = "";
		
		System.out.println("Acierta la palabra");
		do {

			pintarPalabra(caracteresPalabra);

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();

			jugada(caracteresElegidos, caracteresPalabra);

			pintarAhorcado(fallosUsuario);

			if (fallosUsuario >= FALLOS_MAXIMOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}

		} while (!ganarJuego(caracteresPalabra) && fallosUsuario < FALLOS_MAXIMOS);

		input.close();
	}

	private static boolean ganarJuego(char[][] caracteresPalabra) {
		boolean acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado) {
			System.out.println("Has Acertado ");
			return acertado;
		} else {
			return false;
		}
	}

	private static void jugada(String caracteresElegidos, char[][] caracteresPalabra) {
		if(encontrarLetra(caracteresElegidos, caracteresPalabra)) {
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
					}
				}
			}
		} else {
			fallosUsuario++;
		}
	}
	
	private static boolean encontrarLetra(String caracteresElegidos, char[][] caracteresPalabra) {
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					if(caracteresPalabra[1][i] != '1') {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static String elegirPalabraSecreta(String[] palabras) {
		return palabras[(int)(Math.random() * NUM_PALABRAS)];
	}

	private static void pintarPalabra(char[][] caracteresPalabra) {
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

	private static void pintarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

	private static String[] inicializarFichero() {
		File fich = new File(RUTA);
		Scanner inputFichero = null;
		String[] palabras = new String[NUM_PALABRAS];
		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
		
		return palabras;
	}

	private static char[][] inicializarPalabra(String palabraSecreta) {
		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}
	
}