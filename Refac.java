/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

package ra3.refactorizacion; //AÑADIDO PAQUETE

// import java.*; NO SE DEBEN IMPORTAR TODAS LAS CLASES
import java.util.Scanner; //IMPORTADA SOLO LA CLASE SCANNER

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */

public class Refac { //EL NOMBRE DE LA CLASE ES EL MISMO QUE EL DEL FICHERO
	final static String SALUDO = "Bienvenido al programa"; //NOMBRE DE LA CONSTANTE EN MAYÚSCULAS
	
	//AÑADIDO SALTO DE LÍNEA
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); //MODIFICACIÓN DEL NOMBRE DEL IDENTIFICADOR
		
		//UNA SOLA DECLARACIÓN POR LÍNEA E INICIALIZACIÓN DE LA VARIABLE EN EL 
		//MOMENTO DE SU DECLARACIÓN. CAMBIO DEL NOMBRE DEL IDENTIFICADOR.
		System.out.println(SALUDO);
		System.out.println("Introduce tu dni");
		String dni = input.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = input.nextLine();
		
		//UNA SOLA DECLARACIÓN POR LÍNEA, INICIALIZACIÓN DE LA VARIABLE EN EL 
		//MOMENTO DE SU DECLARACIÓN, CORRECCIÓN DEL FORMATO AÑADIENDO ESPACIOS,
		//CORRECCIÓN DE LOS CORCHETES Y MODIFICACIÓN DE LOS NOMBRES DE LOS IDENTIFICADORES
		int numeroA = 7; 
		int numeroB = 16;
		int numeroC = 25;
		if(numeroA > numeroB || numeroC % 5 != 0 && (numeroC * 3 - 1) > numeroB / numeroC) {
			System.out.println("Se cumple la condición");
		}
		
		numeroC = numeroA + numeroB * numeroC + numeroB / numeroA;
		
		//INICIALIZACIÓN DEL ARRAY EN BLOQUE, MODIFIACIÓN DEL NOMBRE DEL 
		//IDENTIFICADOR Y CORRECCIÓN DE LOS CORCHETES
		String[] diasSemana = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
		
		recorrer_array(diasSemana);
	}

	//AÑADIDO SALTO DE LÍNEA ENTRE MÉTODOS Y REUBICACIÓN DE LOS CORCHETES
	static void recorrer_array(String[] vectorDeStrings) {
		//CORRECCIÓN DEL FORMATO AÑADIENDO ESPACIOS
		for(int i = 0; i < vectorDeStrings.length; i++) {
			System.out.println("El dia de la semana en el que te encuentras [ " 
				+ (dia + 1) + " - 7] es el dia: " + vectorDeStrings[dia]);
		}
	}
	
}