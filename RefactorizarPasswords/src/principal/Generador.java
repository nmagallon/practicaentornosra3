package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String password = "";
		mostrarMenu();
		int longitud = longitudPassword(input);
		switch (opcion(input)) {
		case 1:
			for (int i = 0; i < longitud; i++) {
				password += caracterAleatorio();
			}
			break;
		case 2:
			for (int i = 0; i < longitud; i++) {
				password += numeroAleatorio();
			}
			break;
		case 3:
			for (int i = 0; i < longitud; i++) {
				password += generarLetraOCaracterEspecial();
			}
			break;
		case 4:
			for (int i = 0; i < longitud; i++) {
				password += generarLetraONumero();
			}
			break;
		}

		System.out.println(password);
		input.close();
	}

	private static void mostrarMenu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
	
	private static int longitudPassword(Scanner input) {
		System.out.println("Introduce la longitud de la cadena: ");
		return input.nextInt();
	}
	
	private static int opcion(Scanner input) {
		System.out.println("Elige tipo de password: ");
		return input.nextInt();
	}

	static char caracterAleatorio() {
		return (char) ((Math.random() * 26) + 65);
	}
	
	static char caracterEspecialAleatorio() {
		return (char) ((Math.random() * 15) + 33);
	}
	
	static char numeroAleatorio() {
		return (char) ((Math.random() * 10) + 48);
	}
	
	static char generarLetraOCaracterEspecial() {
		int tipoDeCaracter = tipoDeCaracter();
		if (tipoDeCaracter == 1) {
			return caracterAleatorio();
		} else {
			return caracterEspecialAleatorio();
		}
	}
	
	static char generarLetraONumero() {
		int tipoDeCaracter = tipoDeCaracter();
		if (tipoDeCaracter == 1) {
			return caracterAleatorio();
		} else if (tipoDeCaracter == 2) {
			return caracterEspecialAleatorio();
		} else {
			return numeroAleatorio();
		}
	}
	
	static int tipoDeCaracter() {
		return (int) (Math.random() * 3);
	}
}
